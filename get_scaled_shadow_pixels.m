function [newx newy] = get_scaled_shadow_pixels(xCoords,yCoords,img,I)

        [oldr oldc] = size(img);
        I = imresize(img,1/s);
        [newr newc] = size(I);
        pixels = size(xCoords,2);
        newx = zeros(1,pixels);
        newy = zeros(1,pixels);
        for i = 1 : pixels          %% for each shadow pixel
            newx(i) = (xCoords(i)/oldc)*newc;  %% switch row and column when using xml
            newy(i) = (yCoords(i)/oldr)*newr;  %% 
            %disp([xCoords(i) yCoords(i) newx(i) newy(i)]);
        end
        Shadow_xpix{scale+1} = newx;
        Shadow_ypix{scale+1} = newy;
        %%%%% display boundaries
        figure, hold off; imshow(I); hold on;
        plot(newx, newy, '.r', 'MarkerSize', 3);
        drawnow;
end