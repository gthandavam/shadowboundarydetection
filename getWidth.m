function [ wr wg wb ] = getWidth( rArr, gArr, bArr )
%GETWIDTH Returns shadow width for 3 channels
%   Processes the linear array to find the shadow boundaries

    [~, start] = max(rArr); %finding local max and local min 
    [~, stop ] = min(rArr);
    
    wr = abs(start-stop);
    
    
    [~, start] = max(gArr);
    [~, stop ] = min(gArr);
    
    wg = abs(start-stop);
    
    
    [~, start] = max(bArr);
    [~, stop ] = min(bArr);
    
    wb = abs(start-stop);

end

