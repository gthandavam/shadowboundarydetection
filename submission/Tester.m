images  = load('images.mat');
xShadow = load('xShadow.mat');
yShadow = load('yShadow.mat');
xCanny  = load('xCanny.mat');
yCanny  = load('yCanny.mat');

images  = images.images;
xShadow = xShadow.xShadow;
yShadow = yShadow.yShadow;
xCanny  = xCanny.xCanny;
yCanny  = yCanny.yCanny;


trainMap = load('Tuning/trainMap_i2.mat');
trainMap = trainMap.trainMap;

fprintf('Separated %d images for training\n', size(find(trainMap),1));


TestStart = now;

svmStruct = load('Tuning/svmStruct_PartTuned.mat');
svmStruct = svmStruct.svmStruct;


fh = figure;

TP = 0;
FP = 0;
TN = 0;
FN = 0;

for f=1:135
    
    if trainMap(f) == 0
        
        ftMatrix    = [];
        labels      = [];

        
        img = images{f};
        shadowBitmap = zeros(size(img,1), size(img,2));
        resultMap    = zeros(size(img,1), size(img,2));
        
        %TODO: Why xml file has all pixels with .5 ? Rounding off for now
        xShadow{f} = round(xShadow{f});
        yShadow{f} = round(yShadow{f});
        
        for i=1:size(xShadow{f}, 1)
            %Interchanging row column convention
            shadowBitmap(yShadow{f}(i), xShadow{f}(i)) = 1;
        end
    
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %NOTE: Because we are randomizing, svm accuracy will depend on this
        %pick only 300 pixels at random    
    
        
        noPosPixels = min(300, size(xShadow{f}, 1));
        
        randShadow = randi([1 size(xShadow{f}, 1)], noPosPixels, 1);
        
        xCoords = zeros(noPosPixels, 1);
        yCoords = zeros(noPosPixels, 1);
        plabel  = ones(noPosPixels, 1);
        
        %fprintf('identifying shadow pixels\n');
        for i=1:noPosPixels
            xCoords(i) = xShadow{f}(randShadow(i));
            yCoords(i) = yShadow{f}(randShadow(i));
        end
        
        
        
         cannyCtr = 1;
        %considering 300 canny pixels not having a shodow pixel in 100x100
        %neighborhood
%         noNegPixels = min(300, size(xCanny{f},1));
        noNegPixels = noPosPixels;
        xNegCoords  = zeros(noNegPixels, 1);
        yNegCoords  = zeros(noNegPixels, 1);
        nlabel      = zeros(noNegPixels, 1);
        %fprintf('identifying canny pixels\n');
        while cannyCtr <= noNegPixels
            choice = randi([1 size(xCanny{f}, 1)]);
            if considerCannyPixel(10, xCanny{f}(choice), ...
                yCanny{f}(choice), shadowBitmap) == 1
                xNegCoords(cannyCtr) = xCanny{f}(choice);
                yNegCoords(cannyCtr) = yCanny{f}(choice);
                cannyCtr = cannyCtr + 1;
            else 
                %fprintf(' %d %d skipped\n', xCanny{f}(choice),...
                    %yCanny{f}(choice) );
            end
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
%         fprintf(' size of xNegCoords %d, size of xCoords %d\n', ...
%             size(xNegCoords, 1), size(xCoords, 1));
        
        %%FeatureExtraction
        %fprintf('Features for shadow pixels\n');
        features    = getAllFeatures_mod(img, xCoords, yCoords);  

        %Using canny output pixels here
        %fprintf('Features for canny pixels\n');
        negFeatures = getAllFeatures_mod(img, xNegCoords, yNegCoords);    
        
        ftMatrix = vertcat(ftMatrix, features);
        labels   = vertcat(labels, plabel);
        ftMatrix = vertcat(ftMatrix, negFeatures);
        labels   = vertcat(labels, nlabel);
        
        predictedLabels = svmpredict(labels, ftMatrix, svmStruct);
        xResCoords = zeros(size(find(predictedLabels),1), 1);
        yResCoords = zeros(size(find(predictedLabels),1), 1);
        ctr = 1;
        
        for i=1:size(predictedLabels,1)
            if i <= 300 && predictedLabels(i) == 1
                TP = TP + 1;
            elseif i <= 300 && predictedLabels(i) == 0
                FN = FN + 1;
            elseif i > 300 && predictedLabels(i) == 0
                TN = TN + 1;
            else
                % i > 300 and predicted positive
                FP = FP + 1;
            end
            if predictedLabels(i) == 1
                if i <= 300
                    resultMap(yCoords(i), xCoords(i)) = 1;
                else
                    resultMap(yCoords(i-300) , xCoords(i-300)) = 1;
                end
                ctr = ctr + 1;
                
            end
        end
        
        [xResCoords, yResCoords] = postProcess(img, resultMap);
        
        figure(fh); hold off; imshow(img); hold on;
        
%         
%         t=1:length(xResCoords);
%         tt=linspace(t(1),t(end),101);
%         xxResCoords=spline(t,xResCoords,tt);
%         yyResCoords=spline(t,yResCoords,tt);
%         plot(xResCoords,yResCoords,'bs',xxResCoords,yyResCoords)

%             plot(xResCoords,yResCoords,'--rs','LineWidth',2,...
%                 'MarkerEdgeColor','k',...
%                 'MarkerFaceColor','g',...
%                 'MarkerSize',10)

        
        plot(xResCoords, yResCoords, '.r', 'MarkerSize', 8);
        drawnow;
        
%         break;
        
    end
    
end

TestEnd = now;

fprintf(2, 'Time elapsed for Test %s\n', datestr(TestEnd-TestStart, 'HH:MM:SS:FFF'));

fprintf(' TP %d \t FP %d \n TN %d \t FN %d\n', TP, FP, TN, FN );