function [gxGray,gyGray, Ir,magR,gxR,gyR, Ig,magG,gxG,gyG, Ib,magB,gxB,gyB] = getFeature23( I )
    Igray = rgb2gray(I);
%     EdgeCandidates = edge(Igray, 'canny');
    Ir = I(:,:,1);
    Ig = I(:,:,2);
    Ib = I(:,:,3);
    
    [GradientX,GradientY] = gradient(Igray);
%     magGray = sqrt((GradientX.*GradientX)+(GradientY.*GradientY));
%     gxGray=(GradientX./max(max(GradientX)));
    gxGray = GradientX;
%     gyGray=(GradientY./max(max(GradientY)));
    gyGray= GradientY;
%     magGray=(magGray./max(max(magGray)));
    
    [GradientX,GradientY] = gradient(Ir);
    magR = sqrt((GradientX.*GradientX)+(GradientY.*GradientY));
%     gxR=(GradientX./max(max(GradientX)));
%     gyR=(GradientY./max(max(GradientY)));
    gxR=GradientX;
    gyR=GradientY;
    magR=(magR./max(max(magR)));
    
    [GradientX,GradientY] = gradient(Ig);
    magG = sqrt((GradientX.*GradientX)+(GradientY.*GradientY));
    gxG=GradientX;
    gyG=GradientY;
    magG=(magG./max(max(magG)));
    
    [GradientX,GradientY] = gradient(Ib);
    magB = sqrt((GradientX.*GradientX)+(GradientY.*GradientY));
    gxB=GradientX;
    gyB=GradientY;
    magB=(magB./max(max(magB)));