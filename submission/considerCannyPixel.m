function [ ret ] = considerCannyPixel( N, x, y, shadow )
%CONSIDERCANNYPIXEL Returns true when x,y doesnt have a shadow pixel in N X
%N neighborhood
%   shadow() contains info about shadow pixels
    ret = 1;
    
    %x(size(I,1)) for rows, y(size(I,2)) for columns
    x1 = max(1, x - N/2);
    x2 = min(size(shadow,2), x + N/2);
    y1 = max(1, y - N/2);
    y2 = min(size(shadow,1), y + N/2);
    
%     fprintf('x1 %d y1 %d x2 %d y2 %d', x1, y1, x2, y2);
    window = shadow(y1:y2, x1:x2);
    
    if size(find(window)) ~= 0
        ret = 0;
    end
    
end

