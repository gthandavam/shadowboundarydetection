function [ xResCoords1, yResCoords1 ] = postProcess( ~, resultMap )
%POSTPROCESS Performs non-maximal suppression
%   Re-uses canny detector logic

%     SE = strel('line', 10, 45);
%     
%     resultMap = imclose(resultMap, SE);
% 
%     [ yResCoords, xResCoords ] = find(edge(resultMap, 'sobel'));
    
%     [~, edgeim] = edgelink(resultMap);
%     xResCoords1 = [];
%     yResCoords1 = [];
%     
%     for i=1:size(edgelist,2)
%         for j=1:size(edgelist{i},1)
%             xResCoords1 = [xResCoords1 edgelist{i}(j, 2)];
%             yResCoords1 = [yResCoords1 edgelist{i}(j, 1)];
%         end
%         
%     end
%     
%     
%  print(xResCoords1);   

    [ yResCoords1, xResCoords1 ] = find(resultMap>0);
% 
%     N=14;
%     
%     %x(size(I,1)) for rows, y(size(I,2)) for columns
%     
%     x1 = max(1, x - N/2);
%     x2 = min(size(shadow,2), x + N/2);
%     y1 = max(1, y - N/2);
%     y2 = min(size(shadow,1), y + N/2);
%     
% %     fprintf('x1 %d y1 %d x2 %d y2 %d', x1, y1, x2, y2);
%     window = shadow(y1:y2, x1:x2);
%     
%     if size(find(window)) ~= 0
%         ret = 0;
%     end

end

