%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% displayDataset.m
%  Demonstration file for the shadow database. Use this as a starting
%  point, and edit to your liking!
%
% Copyright 2006-2010 Jean-Francois Lalonde
% Carnegie Mellon University
% Consult the LICENSE.txt file for licensing information
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% replace this with your own path
% datasetBasePath = '/nfs/hn26/jlalonde/results/shadowDetection/onlineDb';

% needs these files to load XML files


imgPath = fullfile(datasetBasePath, 'img');
xmlPath = fullfile(datasetBasePath, 'xml');

files = dir(fullfile(imgPath, '*.jpg'));
fh = figure;

fprintf('Found %d images\n', length(files));

ctr = 0;

xShadow = cell(1, 135);
yShadow = cell(1, 135);
xCanny  = cell(1, 135);
yCanny  = cell(1, 135);
images  = cell(1, 135);

for f=1:length(files)
% for f=1:2
    % load image and xml file
    img = imread(fullfile(imgPath, files(f).name));
    
    images{f} = img;
    
% %     
    shadowInfo = load_xml(fullfile(xmlPath, strrep(files(f).name, '.jpg', '.xml')));
% %     
    candidates = getEdgePixelCandidates(rgb2gray(img));
    
    
    % load (x,y) coordinates
    xCoords = arrayfun(@(p) str2double(p.x), shadowInfo.shadowCoords.pt);
    yCoords = arrayfun(@(p) str2double(p.y), shadowInfo.shadowCoords.pt);
    
    xShadow{f} = xCoords';
    yShadow{f} = yCoords';
    
    %find all non-zero entries in a bit-map
    [xEdge, yEdge] = find(candidates);
    
    xCanny{f} = xEdge;
    yCanny{f} = yEdge;
    
    if( size(xCoords) < 300 )
        fprintf('%s deficient', files(f).name);
        ctr = ctr + 1;
    end
    
    
% %     %Uglybitmap, creation - Try to vectorize this operation
% %     shadowMap = zeros(size(img));
% %     for i=1:size(xCoords)
% %         shadowMap(xCoords(i), yCoords(i)) = 1;
% %     end
% %     
    
%     [w h] = size(img);
    
%     %coordinates 100, 100 away from shadow co-ordinate
%     %Circular rotation of indices in MATLAB [since indices start with 1 here]
%     xNeg = 1 + mod( xCoords - 1 + 100, w );
%     yNeg = 1 + mod( yCoords - 1 + 100, h );
    
    % display boundaries
%     figure(fh); hold off; imshow(img); hold on;
%     plot(xCoords, yCoords, '.r', 'MarkerSize', 5);
% %     input('a');
% %     plot(100, 100, '.b', 'MarkerSize', 5);
% %     input('b');
%     drawnow;
end

save('xShadow.mat', 'xShadow');
save('yShadow.mat', 'yShadow');
save('xCanny.mat' , 'xCanny');
save('yCanny.mat' , 'yCanny');
save('images.mat' , 'images');

% fprintf('%d deficient', ctr);
