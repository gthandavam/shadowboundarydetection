function [feature_vector] = getAllFeatures_mod(img, xCoords, yCoords)

Img = im2double(img);
feature_vector = zeros(size(xCoords,1),36);

UBHalfShadowWidth = [16,8,NaN,4]; %for scales 1, half, Not applicable, quarter
GaussianSize = [11,5, NaN, 3];
GaussianScaleSize = [2, 2/sqrt(2), NaN, 0.5];
% IStart = now;
length = size(xCoords,1);
for scale = 0 : 2
    s = scale * 2;
    if(s == 0)
        s = 1;
    end

%     SStart = now;
    I = imresize(Img,1/s);
    [candidate_x candidate_y] = get_scaled_sh_pixels(xCoords,yCoords,img,I);
    [gxGray,gyGray, Ir,magR,gxR,gyR, Ig,magG,gxG,gyG, Ib,magB,gxB,gyB] = getFeature23(I);
    %%featurevectore = zeros(nnz(EdgeCandidates) * 12);
    %nnz(EdgeCandidates);

    weighing_kernel = fspecial('gaussian', GaussianSize(s), GaussianScaleSize(s)); % sigma is optional
%     fprintf('[Scale %d] Starting Feature Extraction..\n',scale);
    for pixel_no = 1 : length
        i = max(1, floor(candidate_y(pixel_no)));
        j = max(1, floor(candidate_x(pixel_no)));
        i = min(i, size(Ir, 1));
        j = min(j, size(Ir, 2));
        
               %------------------ Feature 1 Start------------- 
               dirGray = round(atan2(gyGray(i,j), gxGray(i,j)) * 100) / 100;  
%                fprintf(' %f gradient direction', dirGray)
               if ( (dirGray >= 0 && dirGray < 1.57) || (dirGray >= -3.14 && dirGray < -1.57) )          % 1-3
                   [Hr Lr] = get_Weighted_Avg(Ir,i,j,weighing_kernel, 13);
                   [Hg Lg] = get_Weighted_Avg(Ig,i,j,weighing_kernel, 13);
                   [Hb Lb] = get_Weighted_Avg(Ib,i,j,weighing_kernel, 13);
               elseif( (dirGray >= 1.57 && dirGray <= 3.14) || (dirGray >= -1.57 && dirGray < 0) )       % 2-4
                   [Hr Lr] = get_Weighted_Avg(Ir,i,j,weighing_kernel, 24);
                   [Hg Lg] = get_Weighted_Avg(Ig,i,j,weighing_kernel, 24);
                   [Hb Lb] = get_Weighted_Avg(Ib,i,j,weighing_kernel, 24);
               end
               
               tr = Lr/Hr;
               tg = Lg/Hg;
               tb = Lb/Hb;
               T = (tr + tg + tb)/3;
               Trb = tr/tb;
               Tgb = tg/tb;
               if(isnan(T)) T=0; end
               if(isinf(T)) T=1; end
               if(isnan(Trb)) Trb=0; end
               if(isinf(Trb)) Trb=1; end
               if(isnan(Tgb)) Tgb=0; end
               if(isinf(Tgb)) Tgb=1; end
               feature_vector(pixel_no, scale*12+1) = T;
               feature_vector(pixel_no, scale*12+2) = Trb;
               feature_vector(pixel_no, scale*12+3) = Tgb;
               %------------------ Feature 1 End-------------
               %------------------ Feature 2 Start-----------
               Dr = magR(i,j)/Hr;
               Dg = magG(i,j)/Hg;
               Db = magB(i,j)/Hb;
               if(isnan(Dr))Dr=0;end
               if(isinf(Dr))Dr=1;end
               if(isnan(Dg))Dg=0; end
               if(isinf(Dg))Dg=1;end
               if(isnan(Db))Db=0; end
               if(isinf(Db))Db=1;end
               feature_vector(pixel_no, scale*12+4) = Dr;
               feature_vector(pixel_no, scale*12+5) = Dg;
               feature_vector(pixel_no, scale*12+6) = Db;
               %------------------ Feature 2 End-------------
               %------------------ Feature 3 Start-----------
               dirR = atan2(gyR(i,j), gxR(i,j));
               dirG = atan2(gyG(i,j), gxG(i,j));
               dirB = atan2(gyB(i,j), gxB(i,j));
               Grg = min( abs(dirR-dirG), 6.28 - abs(dirR-dirG));
               Ggb = min( abs(dirG-dirB), 6.28 - abs(dirG-dirB));
               Gbr = min( abs(dirB-dirR), 6.28 - abs(dirB-dirR));
               feature_vector(pixel_no, scale*12+7) = Grg;
               feature_vector(pixel_no, scale*12+8) = Ggb;
               feature_vector(pixel_no, scale*12+9) = Gbr;
               %------------------ Feature 3 End-------------
               %------------------ Feature 4 Start-----------
               %atand accepts only one argument and returns degrees
               %unlike atan2
               dirGrayd = atand(gyGray(i,j) / gxGray(i,j));
               
               if dirGrayd < 0
                   dirGrayd = 180 + dirGrayd;
               end
               
               if isnan(dirGrayd)
                   dirGrayd = 0;
               end
               
               %Ugly implementation of iterating through the pixels
               if ( (dirGrayd >= 0 && dirGrayd < 22.5) || ...
                       (dirGrayd >= 157.5 && dirGrayd <= 180.0) )                   
                   %horizontal stride  
                   
                   jstart = max(1, j - UBHalfShadowWidth(s));
                   jend = min(j + UBHalfShadowWidth(s), size(Ir,2));
                   
                   rArr = Ir(i, jstart:jend);
                   gArr = Ig(i, jstart:jend);
                   bArr = Ib(i, jstart:jend);
                   
                   [wr wg wb] = getWidth(rArr, gArr, bArr);
                   
               elseif (dirGrayd >= 22.5 && dirGrayd < 67.5)
                   %top right bottom left
                   istart = max(1, i - UBHalfShadowWidth(s));
                   jstart = min(j + UBHalfShadowWidth(s), size(Ir,2));
                   istop  = min(i + UBHalfShadowWidth(s), size(Ir,1));
                   jstop  = max(1, j - UBHalfShadowWidth(s));
                   rArr = []; gArr = []; bArr = [];
                   while istart < istop + 1 && jstart > jstop - 1 
                       rArr = [rArr Ir(istart, jstart)];
                       gArr = [gArr Ib(istart, jstart)];
                       bArr = [bArr Ib(istart, jstart)];
                       
                       istart = istart + 1;
                       jstart = jstart - 1;
                       
                   end
                   
                   [wr wg wb] = getWidth(rArr, gArr, bArr);
                    
               elseif (dirGrayd >= 67.5 && dirGrayd < 112.5)
                   %vertical stride  
                   
                   
                   istart = max(1, i - UBHalfShadowWidth(s));
                   iend = min(i + UBHalfShadowWidth(s), size(Ir,1));
                   
                   rArr = Ir(istart:iend, j);
                   gArr = Ig(istart:iend, j);
                   bArr = Ib(istart:iend, j);
                   
                   [wr wg wb] = getWidth(rArr, gArr, bArr);
                    
               elseif (dirGrayd >= 112.5 && dirGrayd < 157.5)
                   %top left bottom right
                   istart = max(1, i - UBHalfShadowWidth(s));
                   jstart = max(1, j - UBHalfShadowWidth(s));
                   istop   = min(i + UBHalfShadowWidth(s), size(Ir,1));
                   jstop   = min(j + UBHalfShadowWidth(s), size(Ir,2));
                   rArr = []; gArr = []; bArr = [];
                   while istart < istop + 1 && jstart < jstop + 1 
                       rArr = [rArr Ir(istart, jstart)];
                       gArr = [gArr Ig(istart, jstart)];
                       bArr = [bArr Ib(istart, jstart)];
                       
                       istart = istart + 1;
                       jstart = jstart + 1;
                       
                   end
                   
                   [wr wg wb] = getWidth(rArr, gArr, bArr);
                                   
               end

               %fprintf('%f %f %f\n', dirGrayd, gyGray(i,j) , gxGray(i,j));
               feature_vector(pixel_no, scale*12+10) = (wr + wg + wb)/3;
               w2 = abs(wr / wg);
               w3 = abs(wr / wb);
               if(isnan(w2))w2 = 0; end
               if(isinf(w2))w2 = 1; end
               feature_vector(pixel_no, scale*12+11) = w2;
               
               if(isnan(w3)) w3=0;end
               if(isinf(w3)) w3=1;end
               feature_vector(pixel_no, scale*12+12) = w3;
               
               %------------------ Feature 4 End-------------
               %filler for feature4
%                 feature_vector(pixel_no, scale*12+10) = 0;
%                 feature_vector(pixel_no, scale*12+11) = 0;
%                 feature_vector(pixel_no, scale*12+12) = 0;

    end
%     SEnd = now;
%     %figure, imshow(I);
%     fprintf(2, 'Time elapsed for a scale %s\n', datestr(SEnd-SStart, 'HH:MM:SS:FFF'));
end


% IEnd = now;
% fprintf(2, 'Time elapsed for 1 image %s\n', datestr(IEnd-IStart, 'HH:MM:SS:FFF'));

