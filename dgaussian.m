function [dg] = dgaussian(N, sigma)
    gaussian = gaussian1D(N, sigma);
    two = int32(2);
    extreme = double(idivide(N, two));
    [X] = (-extreme: extreme);
 
    
    dg = (-X ./ gaussian)/sigma^2;
    
end

