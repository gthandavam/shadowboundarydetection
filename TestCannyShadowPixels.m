

images  = load('images.mat');
xShadow = load('xShadow.mat');
yShadow = load('yShadow.mat');
xCanny  = load('xCanny.mat');
yCanny  = load('yCanny.mat');

images  = images.images;
xShadow = xShadow.xShadow;
yShadow = yShadow.yShadow;
xCanny  = xCanny.xCanny;
yCanny  = yCanny.yCanny;

for f=1:135
    I = rgb2gray(images{f});
    xShadow{f} = floor(xShadow{f});
    yShadow{f} = floor(yShadow{f});
    edges = edge(I, 'canny');
    ctr=0;
    for i=1:size(xShadow{f},1)
%         fprintf(' %d %d \n', yShadow{f}(i),xShadow{f}(i));
        x = xShadow{f}(i);        
        y = yShadow{f}(i);
        if( edges(y,x) == 1 || ... 
            ( (x+1 <= size(I,2) && y+1 <= size(I,1)) ...
                && edges(y+1, x+1) == 1 ) || ...
              (( x-1 >= 1 && y-1 >= 1) && edges(y-1,x-1) == 1 ) )
                
            ctr = ctr + 1;
        end
        
    end 
    fprintf(' shadow %d  shadow in canny %d \n', size(xShadow{f},1), ...
        ctr);
end
