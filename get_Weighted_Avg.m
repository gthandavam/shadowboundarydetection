function [ H L ] = get_Weighted_Avg(Img,i,j,weighing_kernel, quadrant )

        %square kernel
        limit = size(weighing_kernel,1);
        center = ceil(size(weighing_kernel,1) / 2);
    if(i <= limit || j <= limit ...
            || i > size(Img,1) - limit || ...
            j > size(Img,2) - limit )
        H = Img(i,j);
        L = Img(i,j);
        return ;
    end

    if (quadrant == 13)
        avg = conv2(Img(i-limit : i-1 , j+1 : j+limit),weighing_kernel);
        H = avg(center, center);
        avg = conv2(Img(i+1 : i+limit , j-limit : j-1),weighing_kernel);
        L = avg(center, center);
    elseif (quadrant == 24)
        avg = conv2(Img(i-limit : i-1 , j-limit: j-1),weighing_kernel);
        H = avg(center, center);
        avg = conv2(Img(i+1 : i+limit , j+1 : j+limit),weighing_kernel);
        L = avg(center, center);
    else
        H = 0;
        L = 0;
    end
    
    if(H<L)
        temp = L;
        L = H;
        H = temp;
    end
end