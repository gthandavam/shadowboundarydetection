labels   = load('Tuning/labels_i2.mat');
labels   = labels.labels;
ftMatrix = load('Tuning/ftMatrix_i2.mat');
ftMatrix = ftMatrix.ftMatrix;
 
% labels   = labels(1:5000,:);
% ftMatrix = ftMatrix(1:5000,:);
% %grid of parameters
% folds = 5; 
% [C,gamma] = meshgrid(-5:2:15, -15:2:3); 
% %# grid search, and cross-validation 
% cv_acc = zeros(numel(C),1); 
% d= 2;
% for i=1:numel(C)   
%     cv_acc(i) = svmtrain(labels,ftMatrix, ...          
%         sprintf('-c %f -g %f -v %d -t %d', 2^C(i), 2^gamma(i), folds,d));
% end
% %# pair (C,gamma) with best accuracy
% [~,idx] = max(cv_acc); 
% %# contour plot of paramter selection 
% contour(C, gamma, reshape(cv_acc,size(C))), colorbar
% hold on;
% text(C(idx), gamma(idx), sprintf('Acc = %.2f %%',cv_acc(idx)), ...  
%     'HorizontalAlign','left', 'VerticalAlign','top') 
% hold off 
% xlabel('log_2(C)'), ylabel('log_2(\gamma)'), title('Cross-Validation Accuracy') 
% %# now you can train you model using best_C and best_gamma
% best_C = 2^C(idx); best_gamma = 2^gamma(idx); %# ...

%tested online edit feature of bitbucket

bestcv = 0;
for log2c = -1:3,
  for log2g = -4:1,
    cmd = ['-m 2500 -v 5 -c ', num2str(2^log2c), ' -g ', num2str(2^log2g)];
    cv = svmtrain(labels, ftMatrix, cmd);
    if (cv >= bestcv),
      bestcv = cv; bestc = 2^log2c; bestg = 2^log2g;
    end
    fprintf('%g %g %g (best c=%g, g=%g, rate=%g)\n', log2c, log2g, cv, bestc, bestg, bestcv);
  end
end