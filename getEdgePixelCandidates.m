function [ candidates ] = getEdgePixelCandidates( I )
%getEdgePixelCandidates Uses canny edge detector on I

    
%     I = im2double(imread(rgb2gray(img)));
    
    %default sigma in canny = sqrt(2)
    candidates = edge(I, 'canny');
    
    


end

