Shadow Under the sun and the sky

Ameya Patnekar 108997338
Ganesa Thandavam Ponnuraj 108997125


Notes:
#Following definitions may change as we work through the project
Hr, Hg, Hb   : Intensity of a pixel for respective channels, on the lit-side of the candidate edge pixel

Lr, Lg, Lb   : Intensity of a pixel for respective channels, on the darker-side of the candidate edge pixel

sr, sg, sb   : Gradient magnitude of illumination

γr , γg , γb : Gradient direction for respective channels

wr, wg, wb   : Shadow width for respective channels. Start with upper bound of width, and find local minimum and maximum
               along a line in gradient direction. The diff between the extrema location gives the width.
			   Vary the upper bound for different scales. 
