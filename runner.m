%runner for the experiment

run('./startup.m');

img = strcat(datasetBasePath, 'img/flickr-2295970805_b4d4dcfed3_o.jpg');
Img = im2double(imread(img));

UBHalfShadowWidth = [16,8,NaN,4]; %for scales 1, half, Not applicable, quarter

IStart = now;

for scale = 0 : 2
    s = scale * 2;
    if(s == 0)
        s = 1;
    end
    SStart = now;
    I = imresize(Img,1/s);
    [EdgeCandidates, magGray,gxGray,gyGray, Ir,magR,gxR,gyR, Ig,magG,gxG,gyG, Ib,magB,gxB,gyB] = getFeature23(I);
    
    [ r, c ] = size(EdgeCandidates);
    weighing_kernel = fspecial('gaussian', [ 3 3]); % sigma is optional
    fprintf('[Scale %d] Starting Feature Extraction..\n',scale);
    
    for i = UBHalfShadowWidth(s) + 1 : r - UBHalfShadowWidth(s) - 1
        for j = UBHalfShadowWidth(s) + 1 : c - UBHalfShadowWidth(s) - 1
            if EdgeCandidates(i,j)>0
               %------------------ Feature 1 Start------------- 
               dirGray = atan2(gyGray(i,j), gxGray(i,j));               
               if ( (dirGray > 0 && dirGray < 1.57) || (dirGray < -1.57 && dirGray > -3.14) )      % 1-3
                   [Hr Lr] = get_Weighted_Avg(Ir,i,j,weighing_kernel, 13);
                   [Hg Lg] = get_Weighted_Avg(Ig,i,j,weighing_kernel, 13);
                   [Hb Lb] = get_Weighted_Avg(Ib,i,j,weighing_kernel, 13);
               elseif( (dirGray >= 1.57 && dirGray < 3.14) || (dirGray < 0 && dirGray > -1.57) )       % 2-4
                   [Hr Lr] = get_Weighted_Avg(Ir,i,j,weighing_kernel, 24);
                   [Hg Lg] = get_Weighted_Avg(Ig,i,j,weighing_kernel, 24);
                   [Hb Lb] = get_Weighted_Avg(Ib,i,j,weighing_kernel, 24);
               end
               
               tr = Lr/Hr;
               tg = Lg/Hg;
               tb = Lb/Hb;
               T = (tr + tg + tb)/3;
               Trb = tr/tb;
               Tgb = tg/tb;
               %------------------ Feature 1 End-------------
               %------------------ Feature 2 Start-----------
               Dr = magR/Hr;
               Dg = magG/Hg;
               Db = magB/Hb;
               %------------------ Feature 2 End-------------
               %------------------ Feature 3 Start-----------
               dirR = atan2(gyR(i,j), gxR(i,j));
               dirG = atan2(gyG(i,j), gxG(i,j));
               dirB = atan2(gyB(i,j), gxB(i,j));
               Grg = min( abs(dirR-dirG), 6.28 - abs(dirR-dirG));
               Ggb = min( abs(dirG-dirB), 6.28 - abs(dirG-dirB));
               Gbr = min( abs(dirB-dirR), 6.28 - abs(dirB-dirR));
               %------------------ Feature 3 End-------------
               %------------------ Feature 4 Start-----------
               %atand accepts only one argument and returns degrees
               %unlike atan2
               dirGrayd = atand(gyGray(i,j) / gxGray(i,j));
               
               if dirGrayd < 0
                   dirGrayd = 180 + dirGrayd;
               end
               
               %variables to track max min for respective channels
               maxidxr = -1;
               maxidxg = -1;
               maxidxb = -1;
               maxr    = -512;
               maxg    = -512;
               maxb    = -512;
               minidxr = -1;
               minidxg = -1;
               minidxb = -1;
               minr    = 512;
               ming    = 512;
               minb    = 512;
               ctr  = UBHalfShadowWidth(s) * 2 + 1;
               %Ugly implementation of iterating through the pixels
               if ( (dirGrayd >= 0 && dirGrayd < 22.5) || ...
                       (dirGrayd >= 157.5 && dirGrayd < 180.0) )                   
                   %horizontal stride                  
                   runi = i;
                   runj = j - UBHalfShadowWidth(s);
                   for itr = 1:ctr
                       if Ir(runi, runj) > maxr
                           maxr = Ir(runi, runj);
                           maxidxr = itr;
                       end
                       if Ir(runi, runj) < minr
                           minr = Ir(runi, runj);
                           minidxr = itr;
                       end
                       if Ig(runi, runj) > maxg
                           maxg = Ig(runi, runj);
                           maxidxg = itr;
                       end
                       if Ig(runi, runj) < ming
                           ming = Ig(runi, runj);
                           minidxg = itr;
                       end
                       if Ib(runi, runj) > maxb
                           maxb = Ib(runi, runj);
                           maxidxb = itr;
                       end
                       if Ib(runi, runj) < minb
                           minb = Ib(runi, runj);
                           minidxb = itr;
                       end
                       runj = runj + 1;                      
                   end
               elseif (dirGrayd >= 22.5 && dirGrayd < 67.5)
                   %top right bottom left
                   runi = i - UBHalfShadowWidth(s);
                   runj = j + UBHalfShadowWidth(s); 
                   for itr = 1:ctr
                       if Ir(runi, runj) > maxr
                           maxr = Ir(runi, runj);
                           maxidxr = itr;
                       end
                       if Ir(runi, runj) < minr
                           minr = Ir(runi, runj);
                           minidxr = itr;
                       end
                       if Ig(runi, runj) > maxg
                           maxg = Ig(runi, runj);
                           maxidxg = itr;
                       end
                       if Ig(runi, runj) < ming
                           ming = Ig(runi, runj);
                           minidxg = itr;
                       end
                       if Ib(runi, runj) > maxb
                           maxb = Ib(runi, runj);
                           maxidxb = itr;
                       end
                       if Ib(runi, runj) < minb
                           minb = Ib(runi, runj);
                           minidxb = itr;
                       end
                       runi = runi + 1;
                       runj = runj - 1;
                   end 
               elseif (dirGrayd >= 67.5 && dirGrayd < 112.5)
                   %vertical stride   
                   runi = i - UBHalfShadowWidth(s);
                   runj = j; 
                   for itr = 1:ctr
                       if Ir(runi, runj) > maxr
                           maxr = Ir(runi, runj);
                           maxidxr = itr;
                       end
                       if Ir(runi, runj) < minr
                           minr = Ir(runi, runj);
                           minidxr = itr;
                       end
                       if Ig(runi, runj) > maxg
                           maxg = Ig(runi, runj);
                           maxidxg = itr;
                       end
                       if Ig(runi, runj) < ming
                           ming = Ig(runi, runj);
                           minidxg = itr;
                       end
                       if Ib(runi, runj) > maxb
                           maxb = Ib(runi, runj);
                           maxidxb = itr;
                       end
                       if Ib(runi, runj) < minb
                           minb = Ib(runi, runj);
                           minidxb = itr;
                       end
                       runi = runi + 1;
                   end 
               elseif (dirGrayd >= 112.5 && dirGrayd < 157.5)
                   %top left bottom right
                   runi = i - UBHalfShadowWidth(s);
                   runj = j - UBHalfShadowWidth(s); 
                   for itr = 1:ctr
                       if Ir(runi, runj) > maxr
                           maxr = Ir(runi, runj);
                           maxidxr = itr;
                       end
                       if Ir(runi, runj) < minr
                           minr = Ir(runi, runj);
                           minidxr = itr;
                       end
                       if Ig(runi, runj) > maxg
                           maxg = Ig(runi, runj);
                           maxidxg = itr;
                       end
                       if Ig(runi, runj) < ming
                           ming = Ig(runi, runj);
                           minidxg = itr;
                       end
                       if Ib(runi, runj) > maxb
                           maxb = Ib(runi, runj);
                           maxidxb = itr;
                       end
                       if Ib(runi, runj) < minb
                           minb = Ib(runi, runj);
                           minidxb = itr;
                       end
                       runi = runi + 1;
                       runj = runj + 1;
                   end                
               end
               
               wr = abs(maxidxr - minidxr);
               wg = abs(maxidxg - minidxg);
               wb = abs(maxidxb - minidxb);
               %------------------ Feature 4 End-------------
            end
        end
    end
    SEnd = now;
    %figure, imshow(I);
    %figure, imshow(EdgeCandidates);
    fprintf(2, 'Time elapsed for a scale %s\n', datestr(SEnd-SStart, 'HH:MM:SS:FFF'));
    
end

IEnd = now;

fprintf(2, 'Time elapsed for 1 image %s\n', datestr(IEnd-IStart, 'HH:MM:SS:FFF'));
