function [newx newy] = get_scaled_sh_pixels(xCoords,yCoords,img,I)

        [oldr oldc] = size(img);
        [newr newc] = size(I);
        
        pixels = size(xCoords,1);
        newx = zeros(pixels, 1);
        newy = zeros(pixels, 1);
        
        if(newr == oldr && newc == oldc)
            newx = xCoords;
            newy = yCoords;
        else
            for i = 1 : pixels          %% for each shadow pixel
                newx(i) = (xCoords(i)/oldc)*newc;  %% switch row and column when using xml
                newy(i) = (yCoords(i)/oldr)*newr;  %% 
                %disp([xCoords(i) yCoords(i) newx(i) newy(i)]);
            end
        end
%         figure, hold off; imshow(I); hold on;
%         plot(newx, newy, '.r', 'MarkerSize', 3);
%         drawnow;
end