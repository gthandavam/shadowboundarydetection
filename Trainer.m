% % run('./startup.m');
% % 
% % 
% featureMatrix = [];
% 
% images  = load('images.mat');
% xShadow = load('xShadow.mat');
% yShadow = load('yShadow.mat');
% xCanny  = load('xCanny.mat');
% yCanny  = load('yCanny.mat');
% 
% images  = images.images;
% xShadow = xShadow.xShadow;
% yShadow = yShadow.yShadow;
% xCanny  = xCanny.xCanny;
% yCanny  = yCanny.yCanny;
% 
% 
% trainMap = zeros(1, 135)';
% %split train and test here
% 
% %  Out = randi([1 135], 100, 1);
% Out = datasample(1:135,100,'Replace',false);
% for i=Out
%     trainMap(i) = 1;
% end
% % trainMap = load('trainMap.mat');
% % trainMap = trainMap.trainMap;
% 
% fprintf('Separated %d images for training\n', size(find(trainMap),1));
% 
% ftMatrix    = [];
% labels      = [];
% 
% TrainStart = now;
% 
% for f=1:135
%     % load image and xml file
% %     img = imread(fullfile(imgPath, files(f).name));
% %     shadowInfo = load_xml(fullfile(xmlPath, strrep(files(f).name, '.jpg', '.xml')));
%     
%     if trainMap(f) == 1
%         img = images{f};
%         shadowBitmap = zeros(size(img,1), size(img,2));
%         
%         %TODO: Why xml file has all pixels with .5 ? Rounding off for now
%         xShadow{f} = round(xShadow{f});
%         yShadow{f} = round(yShadow{f});
%         
%         for i=1:size(xShadow{f}, 1)
%             %Interchanging row column convention
%             shadowBitmap(yShadow{f}(i), xShadow{f}(i)) = 1;
%         end
%     
%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         %NOTE: Because we are randomizing, svm accuracy will depend on this
%         %pick only 300 pixels at random    
%     
%         
%         noPosPixels = min(300, size(xShadow{f}, 1));
%         
%         randShadow = randi([1 size(xShadow{f}, 1)], noPosPixels, 1);
%         
%         xCoords = zeros(noPosPixels, 1);
%         yCoords = zeros(noPosPixels, 1);
%         plabel  = ones(noPosPixels, 1);
%         
%         %fprintf('identifying shadow pixels\n');
%         for i=1:noPosPixels
%             xCoords(i) = xShadow{f}(randShadow(i));
%             yCoords(i) = yShadow{f}(randShadow(i));
%         end
%         
%         
%         
%          cannyCtr = 1;
%         %considering 300 canny pixels not having a shodow pixel in 100x100
%         %neighborhood
% %         noNegPixels = min(300, size(xCanny{f},1));
%         noNegPixels = noPosPixels;
%         xNegCoords  = zeros(noNegPixels, 1);
%         yNegCoords  = zeros(noNegPixels, 1);
%         nlabel      = zeros(noNegPixels, 1);
%         %fprintf('identifying canny pixels\n');
%         while cannyCtr <= noNegPixels
%             choice = randi([1 size(xCanny{f}, 1)]);
%             if considerCannyPixel(10, xCanny{f}(choice), ...
%                 yCanny{f}(choice), shadowBitmap) == 1
%                 xNegCoords(cannyCtr) = xCanny{f}(choice);
%                 yNegCoords(cannyCtr) = yCanny{f}(choice);
%                 cannyCtr = cannyCtr + 1;
%             else 
%                 %fprintf(' %d %d skipped\n', xCanny{f}(choice),...
%                     %yCanny{f}(choice) );
%             end
%         end
%         
%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         
% %         fprintf(' size of xNegCoords %d, size of xCoords %d\n', ...
% %             size(xNegCoords, 1), size(xCoords, 1));
%         
%         %%FeatureExtraction
%         %fprintf('Features for shadow pixels\n');
%         features    = getAllFeatures_mod(img, xCoords, yCoords);  
% 
%         %Using canny output pixels here
%         %fprintf('Features for canny pixels\n');
%         negFeatures = getAllFeatures_mod(img, xNegCoords, yNegCoords);    
%         
%         ftMatrix = vertcat(ftMatrix, features);
%         labels   = vertcat(labels, plabel);
%         ftMatrix = vertcat(ftMatrix, negFeatures);
%         labels   = vertcat(labels, nlabel);
%         
%  
%     end
%     fprintf('Processed Image %d\n',f);
% end
% 
% TrainEnd = now;
% 
% fprintf(2, 'Time elapsed for Train Features %s\n', datestr(TrainEnd-TrainStart, 'HH:MM:SS:FFF'));
% 
% save('Tuning/trainMap_i2.mat', 'trainMap');
% save('Tuning/ftMatrix_i2.mat', 'ftMatrix');
% save('Tuning/labels_i2.mat'  , 'labels');
% 
% fprintf('finished saving\n');
% 
% 

%Tuning:

%-b 1 for probability estimates
%default uses RBF kernel - think about tuning the RBF kernel
ftMatrix = load('Tuning/ftMatrix_i2.mat');
ftMatrix = ftMatrix.ftMatrix;
labels = load('Tuning/labels_i2.mat');
labels = labels.labels;
TrainStart = now;
svmStruct = svmtrain(labels, ftMatrix, '-h 0 -c 1 -g 0.0625 ');
TrainEnd = now;

fprintf(2, 'Time elapsed for SVM Training %s\n', datestr(TrainEnd-TrainStart, 'HH:MM:SS:FFF'));

save('Tuning/svmStruct_PartTuned.mat', 'svmStruct');


